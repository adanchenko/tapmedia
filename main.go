package main

import (
	"fmt"
	"log"
	"net/http"

	geoip2 "github.com/oschwald/geoip2-golang"
)

func main() {
	geoipDB, err := geoip2.Open("geoip-db/GeoLite2-Country.mmdb")
	if err != nil {
		log.Fatal(err)
		return
	}
	defer geoipDB.Close()

	app := NewApp(geoipDB)

	fmt.Printf("Starting service...\n")
	http.HandleFunc("/", app.HandlerFunc)

	addr := ":8080"
	fmt.Printf("Service started at %s\n", addr)
	http.ListenAndServe(addr, nil)
}
