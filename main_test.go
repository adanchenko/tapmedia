package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	geoip2 "github.com/oschwald/geoip2-golang"
)

func TestService(t *testing.T) {
	tests := []struct {
		testName       string
		fixtureFile    string
		expectedStatus int
		expectedBody   string
	}{
		{
			testName:       "good request",
			fixtureFile:    "good-request.json",
			expectedStatus: http.StatusOK,
			expectedBody:   `{"os":"Windows","device_type":"Desktop","browser":"Chrome","country":"United States","domain":"www.yoursite.com"}`,
		},
		// user agent tests
		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent
		{
			testName:       "request from firefox",
			fixtureFile:    "request-from-firefox.json",
			expectedStatus: http.StatusOK,
			expectedBody:   `{"os":"Mac OS X","device_type":"Desktop","browser":"Firefox","country":"United States","domain":"www.yoursite.com"}`,
		},
		{
			testName:       "request from chrome",
			fixtureFile:    "request-from-chrome.json",
			expectedStatus: http.StatusOK,
			expectedBody:   `{"os":"GNU/Linux","device_type":"Desktop","browser":"Chrome","country":"United States","domain":"www.yoursite.com"}`,
		},
		{
			testName:       "request from opera",
			fixtureFile:    "request-from-opera.json",
			expectedStatus: http.StatusOK,
			expectedBody:   `{"os":"GNU/Linux","device_type":"Desktop","browser":"Opera","country":"United States","domain":"www.yoursite.com"}`,
		},
	}

	app := buildTestApp(t)

	for _, test := range tests {
		t.Run(test.testName, func(t *testing.T) {
			rr := testHandlerFunc(t, http.MethodPost, "/", app.HandlerFunc, test.fixtureFile)

			if rr.Code != test.expectedStatus {
				t.Errorf("handler returned wrong status code: got %v want %v",
					rr.Code, test.expectedStatus)
			}

			if rr.Body.String() != test.expectedBody {
				t.Errorf("handler returned unexpected body: got %v want %v",
					rr.Body.String(), test.expectedBody)
			}
		})
	}
}

func getRequestBodyFixture(t *testing.T, filename string) io.Reader {
	f, err := ioutil.ReadFile(fmt.Sprintf("fixtures/%s", filename))
	if err != nil {
		t.Fatal(err)
	}
	return bytes.NewBuffer(f)
}

func testHandlerFunc(t *testing.T, httpMethod, path string, handlerFunc http.HandlerFunc, fixtureFileName string) *httptest.ResponseRecorder {
	req, err := http.NewRequest(httpMethod, path, getRequestBodyFixture(t, fixtureFileName))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(handlerFunc)

	handler.ServeHTTP(rr, req)
	return rr
}

func buildTestApp(t *testing.T) *App {
	var err error
	geoipDB, err := geoip2.Open("geoip-db/GeoLite2-Country.mmdb")
	if err != nil {
		t.Fatal(err)
	}
	app := NewApp(geoipDB)
	return app
}
