# Tapmedia demo

## Build

``` bash
docker build -t tapmedia-service -f dockerfiles/Dockerfile-service .
```

## Run tests

``` bash
docker run -ti tapmedia-service go test -v
```

## Run service

``` bash
docker run -p 8080:8080 -ti tapmedia-service ./tapmedia
```

## Querying service

``` bash
curl -POST -d @fixtures/good-request.json localhost:8080
```

List of fixtures

``` bash
ls -l fixtures
```