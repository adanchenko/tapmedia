package main

import (
	"encoding/json"
	"net"
	"net/http"
	"net/url"

	geoip2 "github.com/oschwald/geoip2-golang"
	"xojoc.pw/useragent"
)

// App application
type App struct {
	countryProvider CountryProvider
}

// CountryProvider search country by IP
type CountryProvider interface {
	Country(ipAddress net.IP) (*geoip2.Country, error)
}

// NewApp app builder
func NewApp(countryProvider CountryProvider) *App {
	a := App{
		countryProvider: countryProvider,
	}
	return &a
}

// HandlerFunc parse BidRequest
func (a *App) HandlerFunc(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	requestBody := bidRequest{}
	err := decoder.Decode(&requestBody)
	if err != nil {
		writeResponse(w, http.StatusBadRequest, "Unable to decode request body")
		return
	}

	ip := net.ParseIP(requestBody.Device.IP)
	record, err := a.countryProvider.Country(ip)
	if err != nil {
		writeResponse(w, http.StatusBadRequest, "Unable to parse device.ip")
		return
	}

	userAgent := useragent.Parse(requestBody.Device.UserAgent)

	deviceType := detectDeviceType(userAgent)

	pageURL, err := url.Parse(requestBody.Site.Page)
	if err != nil {
		panic(err)
	}

	response := response{
		OS:         userAgent.OS,
		DeviceType: deviceType.String(),
		Browser:    userAgent.Name,
		Country:    record.Country.Names["en"],
		Domain:     pageURL.Host,
	}

	responseBuf, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(responseBuf)
}

func writeResponse(w http.ResponseWriter, status int, message string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	errorResponse := errorResponse{
		Message: message,
	}
	responseBuf, err := json.Marshal(errorResponse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(responseBuf)
}

type errorResponse struct {
	Message string `json:"message"`
}

type bidRequest struct {
	Device device `json:"device"`
	Site   site   `json:"site"`
}

type device struct {
	UserAgent string `json:"ua"`
	IP        string `json:"ip"`
}

type site struct {
	Page string `json:"page"`
}

type response struct {
	OS         string `json:"os"`
	DeviceType string `json:"device_type"`
	Browser    string `json:"browser"`
	Country    string `json:"country"`
	Domain     string `json:"domain"`
}

type deviceType int

const (
	deviceTypeDesktop deviceType = iota
	deviceTypePhone
	deviceTypeTablet
	deviceUnknown
)

func (d deviceType) String() string {
	switch d {
	case deviceTypeDesktop:
		return "Desktop"
	case deviceTypePhone:
		return "Phone"
	case deviceTypeTablet:
		return "Tablet"
	case deviceUnknown:
		return "Unknown"
	default:
		panic("cannot happen")
	}
}

func detectDeviceType(userAgent *useragent.UserAgent) deviceType {
	if userAgent == nil {
		return deviceUnknown
	}
	if userAgent.Mobile {
		return deviceTypePhone
	}
	if userAgent.Tablet {
		return deviceTypeTablet
	}
	return deviceTypeDesktop
}
